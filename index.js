/* ES6 Updates */

/* Exponents */

/*Math.pow() to get the result of a numbe raised to a given exponent */
/*Math.pow(base,exponent)*/
/*
let fivePowerof3 = Math.pow(2, 4);
console.log(fivePowerof3);//125 = 5 x 5 x 5*/
//exponent operators ++ = allow us to get the result of a number raised to a given exponent
let fivePowerOf3 = 2**4;
console.log(fivePowerOf3);


let fivePowerOf2 = 5**2;
console.log("5 raised to 2 is: " + fivePowerOf2);

let fivePowerOf4 = 5**4;
console.log("5 raised to 4 is: " + fivePowerOf4);

let twoPowerOf2 = 2**4;
console.log("2 raised to 4 is: " + twoPowerOf2);


function squareRootChecker(number) {
	return number ** .5 ; 

}

let squareRootof4 = squareRootChecker(4);
console.log(squareRootof4); 

/* Template literals */
/*
	An ES6 Update to craeeting strings. Represented by backticks (``)
	
	"", '' - string literals
	`` - template literals
*/

let sampleString1 = `Charlie`;
let sampleString2 = `Joy`;
console.log(sampleString1);
console.log(sampleString2);

let combinedStrings = `${sampleString1} ${sampleString2}`;
console.log(combinedStrings);


let num1 = 15;
let num2 = 3;
let sentence = `The result of ${num1} plus ${3} is ${num1 + num2}`
let sentenceOld = "The result of " + num1 + " plus " + num2 + " is " + num1+num2;
console.log(sentenceOld);
console.log(sentence);

let sentence2 = (`The result of 15 to the power of 2 is ${15**2}`);
let sentence3 = (`The result of 15 to the power of 2 is ${3*6}`);

console.log(sentence2);
console.log(sentence3);


/* ARRAY DESTRUCTURING
	When we destructure an array, what we essentially do is to save array items in variables

*/
let array = [
	'kobe',
	'lebron',
	'jordan'
];


let goat1 = array[0];
let goat2 = array[1]; 
let goat3 = array[2];
console.log(goat2, goat1, goat3);

let array2 = [
	'curry',
	'lillard',
	'paul',
	'irving'
];

/*ARRAY DESTRUCTURING 
	- ORDER IS IMPORTANT/VARIABLE NAME IS NOT IMPORTANT
*/
let [pg1, pg2, pg3, pg4] = array2;
console.log(pg1);
console.log(pg2);
console.log(pg3);
console.log(pg4);


let array3 = ["Jokic", "Embiid", "Anthony-Towns", "Gobert"];
let [center1,,,center3] = array3;
console.log(center1, center3);

let array4 = ["Draco Malfoy", "Hermione Granger", "Harry Potter", "Ron Weasley", "Professor Snape"];
let [, gryffindor1, gryffindor2, gryffindor3] = array4;
console.log(gryffindor1);
console.log(gryffindor2);
console.log(gryffindor3);

const [slytherin1,,,,slytherin2] = array4;
console.log(slytherin1);
console.log(slytherin2);

gryffindor1 = "Emma Watson";
/*slytherin2 = "Alan Rickman";*/
console.log(gryffindor1);
console.log(slytherin2);


let pokemon = {
	name: "Blastoise",
	level: 40,
	health: 80
}

const pokemon2 = {
	name: "Raichu",
	level: 18,
	health: 44
}

pokemon.name = "Blastoise";
pokemon2.name = "Pikachu";

console.log(pokemon.name);
console.log(pokemon2.name);

let sentence4 = `The pokemon's name is ${pokemon.name}`;
let sentence5 = `It is a level ${pokemon.level} pokemon`;
let sentence6 = `The has at least ${pokemon.health} health points`;
console.log(sentence4);
console.log(sentence5);
console.log(sentence6);


/*

OBJECT DESTRUCTURING 
VARIABLE NAME SHOULD BE THE SAME WITH THE OBJECT.PROPERTYNAME/ORDER IS IRRELEVANT

*/
let {health, name, level} = pokemon;
console.log(health);
console.log(name);
console.log(level);


let person = {
	name: "Paul Phoenix",
	age: 31,
	birthday: "January 12, 1991"
};

function greet(obj) {
	let {name, age, birthday} = obj;
	console.log(`Hi! My name is ${name}`);
	console.log(`I am ${age} years old.`);
	console.log(`My birthday is on ${birthday}`);

}

/*
let hobbies = [
	{
	hobby: "Playing instruments",
	duration: 12
	},
	{
	hobby: "coding",
	duration: 3
	}
];



function enterNewHobby(arrayOfObj) {
	console.log(arrayOfObj);
	let [hobby,] = arrayOfObj;
	console.log(arrayOfObj);

}




function displayHobbies (hobbiesObj) {

}*/
let actors = [
	"hanks",
	"dicarpio",
	"hopkins",
	"reynolds"
];
let director = {
	name: "Alfred Hitchcock",
	birthday: "August 13, 1889",
	isActive: false,
	movies: ["The Birds", "Psycho", "North by Northwest"]
};

let [actor1, actor2, actor3] = actors;

function displayDirector(dir) {
	let {name, birthday, isActive, movies} = dir;
	console.log(`The director's name is ${name}`);
	console.log(`He was born on ${birthday}`);
	console.log(`His movies include:`);
	console.log(`${movies.join(', ')}`);

}
console.log(actor1);
console.log(actor2);
console.log(actor3);

displayDirector(director);

/* ARROW FUNCTIONS
	Arrow functions are an alternative way of writing/declaring functions. there are significant 
	differences between our regular/traditional function and arrow functions

	const functionName = () => {
	console.log("this is an arrow function");
*/

function displayMsg() {
	console.log("mama mo")
}

const displayMsg2 = () => {
	console.log("mama mo walo");
}

displayMsg();
displayMsg2();

function greet(personParams) {
	console.log(`MAMA MO SI ${personParams.name}`);
}
greet(person);

const greet1 = (personParams) => {
console.log(`hello ${personParams.name}`);
}
greet1(person);


let numberArr = [1, 2, 3, 4, 5];

//ANONYMOUS FUNCTION
let multipliedBy = numberArr.map(function(number) {
	return number*5;
})

//ANONYMOUS FUNCTION CONVERTED TO ARROW FUNCTION
let multipliedBy5 = numberArr.map((number) => {

	return number*5;
	console.log(multipliedBy5);
})

//Implicit Return for Arrow Function
/*function addNum(num1, num2) {
	return num1+num2;
}

let sum1 = addNum(5,10);
console.log(sum1);
*/
const subtractNum = (num1, num2) => num1 - num2;
let difference1 = subtractNum(5,2);
console.log(difference1);
//Even without a return keyword, arrow function can return a value. So long as its code block 
//is not wrapped in curly braces. {}


/*Updated AddNum()*/

const addNum = (num1,num2) => num1 + num2;

let sumExample = addNum(110, 10);
console.log(sumExample);


let protagonist = {
	name: "Cloud Strife",
	occupation: "Soldier",
	greet: function() {
		console.log(this);
		console.log(`Hi, I am ${this.name}`);
	},
	introduceJob: () => {
		console.log(this);
		console.log(`I work as a ${this.occupation}`);
	}
}

protagonist.greet();//this refers to parent object
protagonist.introduceJob();/*this.occupation results to undefined
because this. in an arrow function refers to the global window object. */



//Class-Based Objects Blueprints
	//In Javascript, Classes are templates/blueprints to creating objects
	//We can create objects out of the use of Classes.
	//before the introduction of Classes in JS, we mimic this behavior 
	//to creat objects out of templates with the use of constructor functions.

	//Constructor function 

	function Pokemon(name, type, level) {
		this.name = type;
		this.type = name;
		this.level = level;
	}

	let pokemon1 = new Pokemon ("Sandshrew", "Ground", 50);
	console.log(pokemon1);


	/*Es6 Class Creation
		We have a more distinctive way of creating classes instead of just our construction functions*/

			class Car {
				constructor(brand, name, year) {
					this.brand = brand;
					this.name = name;
					this.year = year;
				}
			}

			let car1 = new Car("Toyota", "Vios", "2002");
			
			console.log(car1);


			